from .base import FunctionalTest
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.keys import Keys
import time

TEST_EMAIL = 'imane@mailinator.com'

class LoginTest(FunctionalTest):
 
    def switch_to_new_window(self, text_in_title):
        retries = 60
        while retries > 0:
            for handle in self.browser.window_handles:
                self.browser.switch_to_window(handle)
                if text_in_title in self.browser.title:
                    return 
            retries -= 1
            time.sleep(0.5)
        self.fail('could not find window')

    def test_login_with_persona(self):
        #Edith goes to the awesome superlists site
        #and notice a 'Sign in' link for the first time
        self.browser.get(self.server_url)
        self.browser.find_element_by_id('id_login').click()

        #A Persona login box appears
        self.switch_to_new_window('Mozilla Persona')

        #Edith logs in with her email address
        #Use mockmyid.com for test email
        self.browser.find_element_by_id(
                'authentication_email'
                ).send_keys(TEST_EMAIL)
        self.browser.find_element_by_tag_name('button').click()
        time.sleep(2)
        self.browser.find_element_by_id(
                'authentication_password'
                ).send_keys('12345678')
        time.sleep(2)
        elements = self.browser.find_elements_by_tag_name('button')
        for i in range(len(elements)):
            if elements[i].text == 'sign in':
                elements[i].click()
                break

        #The Persona window closes
        self.switch_to_new_window('To-Do')

        #She can see that she is logged in
        self.wait_for_element_with_id('id_logout')
        navbar = self.browser.find_element_by_css_selector('.navbar')
        self.assertIn(TEST_EMAIL, navbar.text)
        time.sleep(3)
        
        #The persona window closes
        self.switch_to_new_window('To-Do')

        #She can see that she's logged in
        self.wait_to_be_logged_in(email=TEST_EMAIL)
        
        #Refreshing the page, she sees it's a real session login,
        #not just a one-off for that page
        self.browser.refresh()
        self.wait_to_be_logged_in(email=TEST_EMAIL)

        #Terrified of this new feature, she reflexively clicks 'logout'
        self.browser.find_element_by_id('id_logout').click()
        self.wait_to_be_logged_out(email=TEST_EMAIL)

        #The logged out status also persists after refreshinh
        self.browser.refresh()
        self.wait_to_be_logged_out(email=TEST_EMAIL)


