from .base import FunctionalTest
from selenium.webdriver.common.keys import Keys
from selenium import webdriver 

class NewVisitorTest(FunctionalTest):
    
    def test_can_start_a_list_and_retrieve_it_later(self):
        #Edith has heard about a cool new online to do app. She goes to check out its homepage
        self.browser.get(self.server_url)

	#She notices the page title and header mention todo lists
        self.assertIn ('To-Do', self.browser.title)
        header_text = self.browser.find_element_by_tag_name('h1').text
        self.assertIn ('To-Do', header_text)
        
        #She is invited to enter a todo item straight away
        inputBox = self.get_item_input_box()
        self.assertEqual(
                inputBox.get_attribute('placeholder'),
                'Enter a to-do item'
                )

	#She typed "Buy pancakes" into a text box
        inputBox.send_keys('Buy pancakes')

	#When she hits enter, the page updates, and now the page lists
	#"1: Buy pancakes" as an item in a todo list
        inputBox.send_keys(Keys.ENTER)
        edith_list_url = self.browser.current_url 
        self.assertRegex(edith_list_url, '/lists/.+')
        self.check_for_row_in_list_table('1: Buy pancakes')
        
        #There is still a textbox inviting her to add another item, she enters "2: Eat them fresh"
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Eat them fresh')
        inputbox.send_keys(Keys.ENTER)

        #The page updates again, and now shows both items on her list
        self.check_for_row_in_list_table('1: Buy pancakes')
        self.check_for_row_in_list_table('2: Eat them fresh')
            
        #Now a new user, Francis, comes along to the site

        ##We use a new browser session to make sure that no information
        ##of Edith's is coming through from cookies etc
        self.browser.quit()
        self.browser = webdriver.Firefox()
        
        #Francis visits the home page. There is no sign of Edith's
        self.browser.get(self.server_url)
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy pancakes', page_text)
        self.assertNotIn('Eat them fresh', page_text)

        #Francis starts a new list by entering a new item. He
        #is less interesting than Edith
        inputbox = self.get_item_input_box()
        inputbox.send_keys('Wake up')
        inputbox.send_keys(Keys.ENTER)
        
        #Francis gets his own unique url
        francis_list_url = self.browser.current_url
        self.assertRegex(francis_list_url, '/lists/.+')
        self.assertNotEqual(francis_list_url, edith_list_url)

        #Again there's no trace of Edith's list
        page_text = self.browser.find_element_by_tag_name('body').text
        self.assertNotIn('Buy pancakes', page_text)
        self.assertIn('Wake up', page_text)

        # that the site has generated a unique URL for her -- there is some
        # explanatory text to that effect.

        # She visits that URL - her to-do list is still there.


