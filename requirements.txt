Django==1.8
gunicorn==19.3.0
requests==2.8.1
selenium==2.47.3
wheel==0.24.0
